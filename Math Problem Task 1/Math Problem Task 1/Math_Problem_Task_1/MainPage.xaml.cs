﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Math_Problem_Task_1
{
    public partial class MainPage : ContentPage
    {

        static string mathsum = "the sum for the number you entered is : ";
        
        public MainPage()
        {
            InitializeComponent();
        }

        void mathButton(object sender, EventArgs e)
        {

            var a = mathclick.click1(text1.Text);
            label3.Text = (mathsum + a);
        }
    }
}
