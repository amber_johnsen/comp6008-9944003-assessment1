﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RNG_Task_2
{
    
    public partial class MainPage : ContentPage
    {
        static string q1 = "Total Correct Guesses:";
        static string q2 = "Random Number Generated Is:";
        public static string back;

        public MainPage()
        {
            InitializeComponent();
        }

        public void b1Button(object sender, EventArgs e)
        {
            var number = int.Parse(text1.Text);

            var cn = new NumberClick(number);

            var showRandomNumber = cn.GenerateNumber();
            var getColour = cn.GetColour(showRandomNumber);

            Background.BackgroundColor = Color.FromHex($"{getColour}");

            label4.Text = (q2 + showRandomNumber);
            label2.Text = (q1 + NumberClick.Sum);
            text1.Text = "";
        }

    }

}
