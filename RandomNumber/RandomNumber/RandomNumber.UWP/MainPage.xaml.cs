﻿namespace RandomNumber.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            this.InitializeComponent();
            LoadApplication(new RandomNumber.App());
        }
    }
}